import React from 'react';
import { SourceData } from './SourceData';
import { tabularize } from './utilities';

const fields = ["firstName", "lastName", "phone", "email", "bio"]
const mapFields = ["First Name", "Last Name", "Phone", "Email", "Bio"]

// There is only one user in the data source, so we can assume the  
// logged in user is controlled elsewhere and we don't have to filter
// data from multiple users.

export const Profile = () => {
    const context = React.useContext(SourceData)
    const entries = tabularize(context.profile, fields, mapFields)

    return(
        <div className="profile">
            <h2>Profile</h2>
            <div className="wrapper">
                <img src={context.profile.avatarImage} />
                <table>
                    <tbody>
                    {entries.map((entry, index) => {
                        return(                    
                            <tr key={index}>
                                <th>{entry[0]}</th>
                                <td>{entry[1]}</td>
                            </tr>
                        )}
                    )}
                    </tbody>
                </table>
            </div>
        </div>
    )
}