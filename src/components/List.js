import React from 'react';
import { Link } from 'react-router-dom';
import { dateOptions } from './utilities';
import { SourceData } from './SourceData';

export const List = () => {
    const context = React.useContext(SourceData)
    const [ features, setFeatures ] = React.useState(context.data.features.sort((a, b) => b.properties.mag - a.properties.mag))
    const [ sort, setSort ] = React.useState('mag')
 
    const handleSort = e => {
        const attribute = e.target.getAttribute('name')
        if(sort === attribute) {
            setFeatures([...features].reverse());
        } else if(attribute === "title") {
            setFeatures([...features].sort((a, b) => b.properties[attribute] > a.properties[attribute]))
        } else {
            setFeatures([...features].sort((a, b) => b.properties[attribute] - a.properties[attribute]))
        }
        setSort(attribute)
    }

    if(!context) return "Loading..."

    return(
        <div className="quakeList">
            <h2>{context.data.metadata.title}</h2>
            <table>
                <thead>
                    <tr>
                        <th name="title" onClick={handleSort}>{sort === "title" && "*"} Title</th>
                        <th name="mag" onClick={handleSort}>{sort === "mag" && "*"} Magnitude</th>
                        <th name="time" onClick={handleSort}>{sort === "time" && "*"} Time</th>
                    </tr>
                </thead>
                <tbody>
                    {features.map((feature, index) => <LineItem key={index} feature={feature} />)}                    
                </tbody>
            </table>
        </div>
    )
}

const LineItem = ({ feature }) => {
    const datetime = new Date(feature.properties.time).toLocaleString('en-US', dateOptions)
        
    return(
        <tr>
            <td><Link to={`/detail/${feature.id}`}>{feature.properties.title}</Link></td>
            <td className="magnitude">{feature.properties.mag}</td>
            <td>{datetime}</td>
        </tr>
    )
}