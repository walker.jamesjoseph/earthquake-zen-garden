export const dateOptions = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true };

export const tabularize = (data, fields, mapFields = null) => {
    const entries = Object.entries(data)
        .filter(property => fields.includes(property[0]))
        .sort((a,b) => fields.indexOf(a[0]) - fields.indexOf(b[0]))

    if(mapFields) entries.forEach((entry, index) => entry[0] = mapFields[index])

    return entries
}
