import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import { SourceData } from './SourceData';
import { dateOptions, tabularize } from './utilities';

const fields = ["title", "mag", "time", "status", "tsunami", "type"]
const mapFields = ["Title", "Magnitude", "Time", "Status", "Tsunami", "Type"]

export const Detail = () => {
    let match = useRouteMatch();

    const context = React.useContext(SourceData)
    const data = context.data.features.filter(feature => feature.id === match.params.quakeId)[0]
    const entries = tabularize(data.properties, fields, mapFields)

    return(
        <div className="detail">
            <h2>{data.properties.title}</h2>
            <table>
                <tbody>
                    {entries.map((entry, index) => {
                        if(entry[0] === "Time") entry[1] = new Date(entry[1]).toLocaleString('en-US', dateOptions)
                        return(                    
                            <tr key={index}>
                                <th>{entry[0]}</th>
                                <td>{entry[1]}</td>
                            </tr>
                        )}
                    )}
                </tbody>
            </table>
        </div>
    )
}