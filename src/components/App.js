import React from 'react';
import { 
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';
import { SourceDataProvider } from './SourceData';
import { Header } from './Header';
import { List } from './List';
import { Detail } from './Detail';
import { Profile } from './Profile';

export const App = () => {
    return(
        <Router>
            <SourceDataProvider>
                <Header />
                <Switch>
                    <Route exact path="/">
                        <List />
                    </Route>
                    <Route path="/detail/:quakeId">
                        <Detail />
                    </Route>
                    <Route path="/profile">
                        <Profile />
                    </Route>
                </Switch>
            </SourceDataProvider>
        </Router>
    )
}