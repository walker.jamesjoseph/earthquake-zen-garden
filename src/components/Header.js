import React from 'react';
import { Link } from 'react-router-dom';
import { SourceData } from './SourceData';

export const Header = () => {
    const context = React.useContext(SourceData)

    return(
        <nav>
            <Link to="/"><img src={context.site.logoImage} height="40px" alt="Home"/></Link>
            <h1>Earthquake Zen Garden</h1>
            <UserProfile profile={context.profile} />
        </nav>
    )
}

const UserProfile = ({ profile }) =>
    <span className="profileLink"><Link to={`/profile/${profile.firstName}_${profile.lastName}`}>Welcome {profile.firstName}</Link></span>
