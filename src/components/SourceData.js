import React from 'react';
import { data } from './data.js';

const SourceData = React.createContext()

const SourceDataProvider = ({ children }) =>
    <SourceData.Provider value={data}>
        {children}
    </SourceData.Provider>

export { SourceData, SourceDataProvider }