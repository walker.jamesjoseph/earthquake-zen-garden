import React from 'react';
import { MemoryRouter, Route } from "react-router-dom";
import { fireEvent, render, screen, within } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { SourceDataProvider } from '../components/SourceData.js';
import { List } from '../components/List';

const renderWithContext = component => {
    return render(
        <SourceDataProvider>
            <MemoryRouter>
                <Route>
                    {component}
                </Route>
            </MemoryRouter>
        </SourceDataProvider>
    )
}

beforeEach(() => { 
    renderWithContext(<List />) 
})

test('Clicking on the title sorts the list', () => {
    const table = screen.getByRole('table')
    const heading = screen.getByRole('columnheader', { name: /title/i })
    fireEvent.click(heading)
    expect(within(table).getAllByRole('row')[1]).toContainHTML('M 5.3 - 24km ESE of Nemuro, Japan')
    fireEvent.click(heading)
    expect(within(table).getAllByRole('row')[1]).toContainHTML('M 0.5 - 11km NE of Aguanga, CA')
})

test('Clicking on the magnitude sorts the list', () => {
    const table = screen.getByRole('table')
    const heading = screen.getByRole('columnheader', { name: /mag/i })
    fireEvent.click(heading)
    expect(within(table).getAllByRole('row')[1]).toContainHTML('0.54')
    fireEvent.click(heading)
    expect(within(table).getAllByRole('row')[1]).toContainHTML('5.3')
})

test('Clicking on the time sorts the list', () => {
    const table = screen.getByRole('table')
    const heading = screen.getByRole('columnheader', { name: /time/i })
    fireEvent.click(heading)
    expect(within(table).getAllByRole('row')[1]).toContainHTML('Apr 13, 2018, 3:25 PM')
    fireEvent.click(heading)
    expect(within(table).getAllByRole('row')[1]).toContainHTML('Apr 13, 2018, 2:39 PM')
})