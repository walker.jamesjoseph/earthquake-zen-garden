import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { SourceDataProvider } from '../components/SourceData.js';
import { Profile } from '../components/Profile';

const renderWithContext = component => {
    return render(<SourceDataProvider>{component}</SourceDataProvider>)
}

test('Profile renders', () => {
    const container = renderWithContext(<Profile />)
    expect(container.getByText(/wang/i)).toBeInTheDocument()
})

