import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { App } from '../components/App';

beforeEach(() => {
    render(<App />);
})

test('App renders', () => {
    expect(screen.getByText(/Earthquake Zen Garden/)).toBeInTheDocument()
})

test('User can navigate to profile and back', () => {
    const welcomeMessage = screen.getByText(/welcome/i)
    fireEvent.click(welcomeMessage)
    expect(screen.getByRole('heading', { level: 2 })).toHaveTextContent("Profile")
    fireEvent.click(screen.getByAltText("Home"))
    expect(screen.getByRole('heading', { level: 2 })).toHaveTextContent("USGS All Earthquakes, Past Hour")
})

test('User can navigate to detail and back', () => {
    fireEvent.click(screen.getByText(/nemuro/i))
    expect(screen.getByRole('heading', { level: 2 })).toHaveTextContent("M 5.3 - 24km ESE of Nemuro, Japan")
    fireEvent.click(screen.getByAltText("Home"))
    expect(screen.getByRole('heading', { level: 2 })).toHaveTextContent("USGS All Earthquakes, Past Hour")
})
